sts-assume-role
===============

An Ansible Role that makes using assume role easier.

Requirements
------------

You must have cross account roles setup within your AWS environment for this to be able to work.

Role Variables
--------------

Since each environment will be unique, no default variables are set.

The assumption is that this role will be used over multiple accounts, so the easiest way to set the accounts variable is to set it globally so that all your AWS account information is centrally located.  Generally, setting it in a global group_vars directory is preferred.

    accounts:
        prototype:    123456789012
        development:  234567890123
        staging:      345678901234
        prod:         456789012345

Another variable that is a good candidate for a global variable file is the aws_region. Since this is generally used for running a number of AWS commands, setting it in the localhost host_var file can be ideal.  The following configures the variable to use the local environmental variable defined by AWS, and if that is not set defaults to us-east-1:

    aws_region : "{{lookup('env','AWS_DEFAULT_REGION') |default('us-east-1',true)}}"

You can set the remaining two variables either in the account specific inventory file or in the playbook.  The account_name variable should match the key in the accounts variable and the iam_role should match the name of the role you will be using to run the AWS commands.

    account_name: prototype
    iam_role: ansible

You can also optionally change the name of the variable that holds the creds from the default (shown below):

    env_var: env

Dependencies
------------

None.

Example Playbook
----------------

Include the credentials setup block at the beginning of your playbook, then in subsequent blocks include the environment directive to pass your credentials in.


    - name: credentials setup
      hosts: localhost
      gather_facts: no
      roles:
        - role: austincloudguru.sts-assume-role
          account_name: prototype
          iam_role: sre

    - name: Build AMIs
      hosts: localhost
      environment: '{{ env }}'
      roles:
        - austincloudguru.image-builder


License
-------

MIT

Author Information
------------------

Mark Honomichl aka [AustinCloudGuru](https://austincloud.guru)

Created in 2017